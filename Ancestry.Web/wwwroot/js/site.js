﻿// Write your JavaScript code.

var app;
(function () {
    app = angular.module("myApp", []);
})();  

app.service("APIService", function ($http) {
    this.getSubs = function (name, male, female, page) {
        return $http.get("http://localhost:58478/api/people/" + name + "/" + male + "/" + female + "/" + page);
    }
});

app.controller('formCtrl',
    function($scope, APIService) {
        $scope.Male = false;
        $scope.Female = false;
        $scope.currPage = 1;
        $scope.Pages = 0;
        //$scope.currPage = setPage(1);
        $scope.submit = function() {
            getAll();
        };

        $scope.setPage = function (page) {
            if (page < 1 || page > $scope.pager.totalPages) {
                return;
            }
            $scope.pager = $scope.GetPager($scope.totalItems, page, 10);
            $scope.people = get($scope.maxData);
        }

        $scope.GetPager = function (totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;

            // default page size is 10
            pageSize = pageSize || 10;

            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            var pages = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        };

        function getAll() {
            var servCall = APIService.getSubs($scope.Name, $scope.Male, $scope.Female, $scope.currPage);
            servCall.then(function (d) {
                    $scope.maxData = d.data;
                    $scope.pager = $scope.GetPager(d.data.length, $scope.currPage, 10);
                    $scope.people = get(d.data);
                },
                function(error) {
                    alert('Oops! Something went wrong while fetching the data.');
                });
        }

        function get(s) {
            var start = $scope.pager.currentPage * 10;
            var temp = [];
            for (var i = start - 10; i < start; i++) {
                if (i <= $scope.maxData.length) {
                    temp.push(s[i]);
                } else {
                    break;
                }
            }
            return temp;
        }
    }

);
//function PagerService() {
//    // service definition
//    var service = {};

//    service.GetPager = GetPager;

//    return service;

//    // service implementation
//    function GetPager(totalItems, currentPage, pageSize) {
//        // default to first page
//        currentPage = currentPage || 1;

//        // default page size is 10
//        pageSize = pageSize || 10;

//        // calculate total pages
//        var totalPages = Math.ceil(totalItems / pageSize);

//        var startPage, endPage;
//        if (totalPages <= 10) {
//            // less than 10 total pages so show all
//            startPage = 1;
//            endPage = totalPages;
//        } else {
//            // more than 10 total pages so calculate start and end pages
//            if (currentPage <= 6) {
//                startPage = 1;
//                endPage = 10;
//            } else if (currentPage + 4 >= totalPages) {
//                startPage = totalPages - 9;
//                endPage = totalPages;
//            } else {
//                startPage = currentPage - 5;
//                endPage = currentPage + 4;
//            }
//        }

//        // calculate start and end item indexes
//        var startIndex = (currentPage - 1) * pageSize;
//        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

//        // create an array of pages to ng-repeat in the pager control
//        var pages = 100;//_.range(startPage, endPage + 1);

//        // return object with all pager properties required by the view
//        return {
//            totalItems: totalItems,
//            currentPage: currentPage,
//            pageSize: pageSize,
//            totalPages: totalPages,
//            startPage: startPage,
//            endPage: endPage,
//            startIndex: startIndex,
//            endIndex: endIndex,
//            pages: pages
//        };
//    }
//};
//function setPage($scope, page) {
//    if (page < 1 || page > $scope.pager.totalPages) {
//        return;
//    }

//    // get pager object from service
//    $scope.pager = PagerService.GetPager($scope.dummyItems.length, page);

//    // get current page of items
//    $scope.items = $scope.dummyItems.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
//};
