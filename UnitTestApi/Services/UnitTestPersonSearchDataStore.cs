﻿using Ancestry.Api.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestApi.Services
{
    [TestClass]
    public class UnitTestPersonSearchDataStore
    {
        [TestMethod]
        public void Test_GetData_IsValid()
        {
            // arrange
            var ds = new PersonSearchDataStore(null);
            ds.Url = "http://localhost:57459/data/data_small.json";
            // act
            var result = ds.GetData();
            // assert
            Assert.IsNotNull(result);
        }
    }
}