﻿using Ancestry.Api.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestApi.Services
{
    [TestClass]
    public class UnitTestPersonSearchService
    {
        [TestMethod]
        public void Test_Get_IsMale_Person()
        {
            // arrange
            var personSearchService = new PersonSearchService(null);
            // act
            var result = personSearchService.Get("Giorgia Ina", true,false,1);
            // assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_Get_IsFemale_Person()
        {
            // arrange
            var personSearchService = new PersonSearchService(null);
            // act
            var result = personSearchService.Get("Siana Lissa", false, true, 1);
            // assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_Get_IsUnknown()
        {
            // arrange
            var personSearchService = new PersonSearchService(null);
            // act
            var result = personSearchService.Get("Siana Lissa", false, false, 1);
            // assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_Get_IsBoth()
        {
            // arrange
            var personSearchService = new PersonSearchService(null);
            // act
            var result = personSearchService.Get("Siana Lissa", true, true, 1);
            // assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_Get_FirstHundredRecords()
        {
            // arrange
            var personSearchService = new PersonSearchService(null);
            // act
            var result = personSearchService.Get("S", true, true, 1);
            // assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Test_Get_SecondHundredRecords()
        {
            // arrange
            var personSearchService = new PersonSearchService(null);
            // act
            var result = personSearchService.Get("S", true, true, 2);
            // assert
            Assert.IsNotNull(result);
        }
    }
}
