﻿using System.Collections.Generic;
using System.Linq;
using Ancestry.Api.Models;
using Microsoft.Extensions.Options;

namespace Ancestry.Api.Services
{
    public class PersonSearchService : IPersonSearchService
    {
        private IOptions<ApplicationSettings> _settings;
        public PersonSearchService(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
        }
        /// <summary>
        /// Get people based on search criteria
        /// </summary>
        /// <param name="name">name searched</param>
        /// <param name="isMale"></param>
        /// <param name="isFemale"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<PersonSearchDetails> Get(string name, bool isMale, bool isFemale, int page)
        {
            var data = new PersonSearchDataStore(_settings).GetData();
            var people = data.people.Where(x => (x.name).Contains(name));
            var places = data.places;

            var searchResult = new List<Person>();
            if (!isMale && !isFemale)
            {
                isMale = true;
                isFemale = true;
            }

            var list = people.ToList();
            if (isMale)
            {
                searchResult = list.Where(x => x.gender.ToUpper() == "M").ToList();
            }
            if (isFemale)
            {
                searchResult = list.Where(x => x.gender.ToUpper() == "F").ToList();
            }

            // maxRecordCount will limit the result set to 100 items, for search results with pagination with 10 rows per page.
            // 10 pages can be displayed on page. the next series would require an api call.
            var pageSize = 100;   // TODO: refactor configurable
            var skip = pageSize * (page - 1);
            
            var maxResult = searchResult.Skip(skip).Take(pageSize);
            return maxResult.Select(item => new PersonSearchDetails()
                {
                    Id = item.id,
                    Name = item.name,
                    Gender = item.gender,
                    BirthPlace = (item.place_id != null) ? places.SingleOrDefault(x => x.id == item.place_id)?.name : null
                })
                .ToList();
        }
    }
}
