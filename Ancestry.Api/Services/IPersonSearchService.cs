﻿using System.Collections.Generic;
using Ancestry.Api.Models;

namespace Ancestry.Api.Services
{
    public interface IPersonSearchService
    {
        IEnumerable<PersonSearchDetails> Get(string name, bool isMale, bool isFemale, int paging);
    }
}
