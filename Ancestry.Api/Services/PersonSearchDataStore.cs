﻿using System.IO;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Ancestry.Api.Services
{
    public class PersonSearchDataStore
    {
        public string Url { get; set; } = "http://localhost:57459/data/data_small.json";
        private IOptions<ApplicationSettings> _settings;
        public PersonSearchDataStore(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
        }
        /// <summary>
        /// Get person ancestry data from data provider
        /// </summary>
        /// <returns></returns>
        public Models.Ancestry GetData()
        {
            // get data url
            if (_settings!=null) Url =  (string.IsNullOrEmpty(_settings.Value.DataUrl)) ? Url : _settings.Value.DataUrl;
            HttpClient client = new HttpClient();
            Models.Ancestry ancestry;
            // stream json
            using (Stream s = client.GetStreamAsync(Url).Result)
            using (StreamReader sr = new StreamReader(s))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                JsonSerializer serializer = new JsonSerializer();

                // read the json from a stream
                // json size doesn't matter because only a small piece is read at a time from the HTTP request
                ancestry = serializer.Deserialize<Models.Ancestry>(reader);
            }
            return ancestry;
        }
    }
}
