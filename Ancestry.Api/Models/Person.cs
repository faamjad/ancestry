﻿using System.Runtime.Serialization;

namespace Ancestry.Api.Models
{
    /// <summary>
    /// Person details
    /// </summary>
    [DataContract]
    public class Person
    {
        /// <summary>
        /// Id for the person
        /// </summary>
        [DataMember]
        public int id { get; set; }
        /// <summary>
        /// Name of the person
        /// </summary>
        [DataMember]
        public string name { get; set; }
        /// <summary>
        /// Gender of the person
        /// </summary>
        [DataMember]
        public string gender { get; set; }
        /// <summary>
        /// Id reference to father of current person from person details
        /// </summary>
        [DataMember]
        public int? father_id { get; set; }
        /// <summary>
        /// Id reference to mother of current person from the person details
        /// </summary>
        [DataMember]
        public int? mother_id { get; set; }
        /// <summary>
        /// Id reference to the place of current person from place details
        /// </summary>
        [DataMember]
        public int? place_id { get; set; }
        /// <summary>
        /// heirarch level
        /// </summary>
        [DataMember]
        public int? level { get; set; }
    }
}

