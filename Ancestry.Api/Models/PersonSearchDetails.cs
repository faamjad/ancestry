﻿using System.Runtime.Serialization;

namespace Ancestry.Api.Models
{
    [DataContract]
    public class PersonSearchDetails
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string BirthPlace { get; set; }
    }
}
