﻿using System.Runtime.Serialization;

namespace Ancestry.Api.Models
{
    /// <summary>
    /// Place details
    /// </summary>
    [DataContract]
    public class Place
    {
        /// <summary>
        /// Id for the place
        /// </summary>
        [DataMember]
        public int id { get; set; }
        /// <summary>
        /// Name of the place / address
        /// </summary>
        [DataMember]
        public string name { get; set; }
    }
}
