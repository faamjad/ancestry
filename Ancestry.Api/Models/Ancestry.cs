﻿using System.Collections.Generic;

namespace Ancestry.Api.Models
{
    /// <summary>
    /// Ancestry details
    /// </summary>
    public class Ancestry
    {
        /// <summary>
        /// List of places / birth place
        /// </summary>
        public List<Place> places { get; set; }
        /// <summary>
        /// List of people in the digital ancestry 
        /// </summary>
        public List<Person> people { get; set; }
    }
}
