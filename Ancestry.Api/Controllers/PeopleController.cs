﻿using Ancestry.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Ancestry.Api.Controllers
{
    [Route("api/people")]
    public class PeopleController : Controller
    {
        private IOptions<ApplicationSettings> _settings;
        public PeopleController(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
        }

        [HttpGet("{name}/{isMale}/{isFemale}/{page}")]
        public IActionResult GetPeople(string name, bool isMale, bool isFemale, int page)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serv = new PersonSearchService(_settings);
            var result = serv.Get(name, isMale, isFemale, page);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}