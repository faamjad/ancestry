﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace Ancestry.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add service and create Policy with options
            // NOTE: for secured access define appropriate policy settings. AllowAnyOrigin set for demo purposes only.
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            services.AddMvc();
            services.AddOptions();

            // configure application settings
            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // use developer exception page when an exception is encountered.
            // environment can be configured by updating the environment variable.
            // go to project properties --> Debug --> Environment Variables
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else // use exception handler
            {
                app.UseExceptionHandler();
            }
            // global policy - assign here or on each controller
            app.UseCors("CorsPolicy");
            app.UseStatusCodePages();
            app.UseMvc();
        }
    }
}
