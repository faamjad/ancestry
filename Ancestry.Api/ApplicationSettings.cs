﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ancestry.Api
{
    public class ApplicationSettings
    {
        public string DataUrl { get; set; }
    }
}
